#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <avr/eeprom.h>
#include <avr/wdt.h>
#include <avr/io.h>

#include "usbdrv.h"
#include "oddebug.h"
#include "irmp/irmp.h"
#include "hid_keys.h"

#define nop()   __asm__ __volatile__ ("nop");
#define reset() __asm__ __volatile__ ("rjmp 0");

#define led_on()  do { PORTD |= _BV(6); }    while (0)
#define led_off() do { PORTD &= ~_BV(6); }   while (0)
#define led_tog() do { PORTD ^= _BV(6); }    while (0)

#define lir_on()  do { PORTD |= _BV(3); }    while (0)
#define lir_off() do { PORTD &= ~_BV(3); }   while (0)
#define lir_tog() do { PORTD ^= _BV(3); }    while (0)

#define usb_1_on()  do { PORTB |= _BV(0); }  while (0)
#define usb_1_off() do { PORTB &= ~_BV(0); } while (0)
#define usb_1_tog() do { PORTB ^= _BV(0); }  while (0)

#define usb_2_on()  do { PORTB |= _BV(1); }  while (0)
#define usb_2_off() do { PORTB &= ~_BV(1); } while (0)
#define usb_2_tog() do { PORTB ^= _BV(1); }  while (0)

#define usb_3_on()  do { PORTD |= _BV(7); }  while (0)
#define usb_3_off() do { PORTD &= ~_BV(7); } while (0)
#define usb_3_tog() do { PORTD ^= _BV(7); }  while (0)

#define BUTTON_DOWN  (!(PIND & _BV(PD5)))

#define IR_READY_TO_SAVE 1

// This value need to match of below title elements and swith statement for ir command parsing block (line 591).
#define CODES_TO_TRAIN 11
char *cmd_title[CODES_TO_TRAIN] = { "ON/OFF", "PORT 1", "PORT 2", "PORT 3", "UP", "DOWN", "LEFT", "RIGHT", "ESCAPE", "ENTER", "TAB" };

static uchar keyboardBuffer[5] = { 1, 0,0,0,0, };  // kbd
static uchar mouseBuffer[4] = { 2, 0,0,0, };  // mouse
//static uchar ir_timeout = 0;
static uchar idleRate = 0;                       // in 4 ms units
static uchar tx_mouse = 0;
static uchar tx_kbd = 0;

static IRMP_DATA irmp_data;
static IRMP_DATA last_irmp_data;
static IRMP_DATA trained_irmp_data[CODES_TO_TRAIN] EEMEM;

// mouse acceleration values
// This part will be used when I implement mouse movement.
/*
#define IR_ACCEL_DEF    20
#define IR_ACCEL_INC_LO 1
#define IR_ACCEL_INC_HI 2
#define IR_ACCEL_MAX    100

static uchar ir_accel = IR_ACCEL_DEF;
static uchar ir_data = 0;
*/

static const char hidReportDescriptor0[] PROGMEM = {
    /* partial keyboard */
    0x05, 0x01, /* Usage Page (Generic Desktop), */
    0x09, 0x06, /* Usage (Keyboard), */
    0xA1, 0x01, /* Collection (Application), */
    0x85, 0x01,     /* Report Id (1) */
    0x95, 0x04,     /* Report Count (4), */
    0x75, 0x08,     /* Report Size (8), */
    0x15, 0x00,     /* Logical Minimum (0), */
    0x25, 0x65,     /* Logical Maximum(101), */
    0x05, 0x07,     /* Usage Page (Key Codes), */
    0x19, 0x00,     /* Usage Minimum (0), */
    0x29, 0x65,     /* Usage Maximum (101), */
    0x81, 0x00,     /* Input (Data, Array),               ;Key arrays (4 bytes) */
    0xC0,       /* End Collection */

    /* mouse */
    0x05, 0x01, /* Usage Page (Generic Desktop), */
    0x09, 0x02, /* Usage (Mouse), */
    0xA1, 0x01, /* Collection (Application), */
    0x09, 0x01, /*   Usage (Pointer), */
    0xA1, 0x00, /*   Collection (Physical), */
    0x05, 0x09,     /* Usage Page (Buttons), */
    0x19, 0x01,     /* Usage Minimum (01), */
    0x29, 0x03,     /* Usage Maximun (03), */
    0x15, 0x00,     /* Logical Minimum (0), */
    0x25, 0x01,     /* Logical Maximum (1), */
    0x85, 0x02,     /* Report Id (2) */
    0x95, 0x03,     /* Report Count (3), */
    0x75, 0x01,     /* Report Size (1), */
    0x81, 0x02,     /* Input (Data, Variable, Absolute), ;3 button bits */
    0x95, 0x01,     /* Report Count (1), */
    0x75, 0x05,     /* Report Size (5), */
    0x81, 0x01,     /* Input (Constant),                 ;5 bit padding */
    0x05, 0x01,     /* Usage Page (Generic Desktop), */
    0x09, 0x30,     /* Usage (X), */
    0x09, 0x31,     /* Usage (Y), */
    0x15, 0x81,     /* Logical Minimum (-127), */
    0x25, 0x7F,     /* Logical Maximum (127), */
    0x75, 0x08,     /* Report Size (8), */
    0x95, 0x02,     /* Report Count (2), */
    0x81, 0x06,     /* Input (Data, Variable, Relative), ;2 position bytes (X & Y) */
    0xC0,       /*   End Collection, */
    0xC0,       /* End Collection */
};

static const char configDescriptor[] PROGMEM = {    /* USB configuration descriptor */
    9,          /* sizeof(usbDescriptorConfiguration): length of descriptor in bytes */
    USBDESCR_CONFIG,    /* descriptor type */
    9 + 1 * (9 + 9 + 7), 0,
    /* total length of data returned (including inlined descriptors) */
    1,          /* number of interfaces in this configuration */
    1,          /* index of this configuration */
    0,          /* configuration name string index */
    USBATTR_BUSPOWER,   /* attributes */
    USB_CFG_MAX_BUS_POWER / 2,  /* max USB current in 2mA units */

    /* --- interface 0 --- */
    9,          /* sizeof(usbDescrInterface): length of descriptor in bytes */
    USBDESCR_INTERFACE, /* descriptor type */
    0,          /* index of this interface */
    0,          /* alternate setting for this interface */
    1,          /* endpoints excl 0: number of endpoint descriptors to follow */
    3,          /* 3=HID class */
    0,          /* subclass: 0=none, 1=boot */
    0,          /* protocol: 0=none, 1=keyboard, 2=mouse */
    0,          /* string index for interface */

    /* HID descriptor */
    9,          /* sizeof(usbDescrHID): length of descriptor in bytes */
    USBDESCR_HID,       /* descriptor type: HID */
    0x01, 0x01,     /* BCD representation of HID version */
    0x00,           /* target country code */
    0x01,           /* number of HID Report (or other HID class) Descriptor infos to follow */
    0x22,           /* descriptor type: report */
    sizeof(hidReportDescriptor0), 0,    /* total length of report descriptor */

    /* endpoint descriptor for endpoint 1 */
    7,          /* sizeof(usbDescrEndpoint) */
    USBDESCR_ENDPOINT,  /* descriptor type = endpoint */
    0x81,           /* IN endpoint number 1 */
    0x03,           /* attrib: Interrupt endpoint */
    8, 0,           /* maximum packet size */
    USB_CFG_INTR_POLL_INTERVAL, /* in ms */
};

static void gpio_init(void){

    // Activate all pull-ups.
    PORTB = 0xff;
    PORTC = 0xff;
    PORTD = 0xff;
    // All pins input.
    DDRB = 0;
    DDRC = 0;
    DDRD = 0; 

    DDRB |= _BV(0) | _BV(1);
    DDRD |= _BV(6) | _BV(7) | _BV(3);;

    led_off();
    lir_off();
    usb_1_off();
    usb_2_off();
    usb_3_off();
}

static void timer_init(void){

  TCCR0 = 5;

  //TCCR0 |= (1 << CS01) | (1 << CS00);
  /* IR polling timer */
  // switch CTC Mode on, set prescaler to 1
  TCCR1B  = (1 << WGM12) | (1 << CS10);

  // may adjust IR polling rate here to optimize IR receiving:
  // compare value: 1/10000 of CPU frequency
  OCR1A   =  (F_CPU / F_INTERRUPTS) - 1;

  // enable Timer1 for IR polling
  #if defined (__AVR_ATmega8__) || defined (__AVR_ATmega16__) || defined (__AVR_ATmega32__) || defined (__AVR_ATmega64__) || defined (__AVR_ATmega162__)
    // Timer1A ISR activate
    TIMSK = 1 << OCIE1A;  
  #else
    // Timer1A ISR activate        
    TIMSK1 = 1 << OCIE1A;
  #endif
}

static void usb_init(void){

    USBOUT ^= _BV (USB_CFG_DMINUS_BIT) | _BV (USB_CFG_DPLUS_BIT);

    // enforce re-enumeration, do this while interrupts are disabled!
    usbDeviceDisconnect();

    uchar i = 0;

    // fake USB disconnect for > 500 ms
    while(--i){
        wdt_reset();
        _delay_ms(5);
    }
    usbDeviceConnect();
    usbInit();
    /* configure timer 0 for a rate of 12M/(1024 * 256) = 45.78 Hz (~22ms) */
    //TCCR0 = 5;      /* timer 0 prescaler: 1024 */
}


uchar usbFunctionDescriptor(usbRequest_t * rq) {

    uchar *p = 0, len = 0;

    if (rq->wValue.bytes[1] == USBDESCR_CONFIG) {
        p = (uchar *) configDescriptor;
        len = sizeof(configDescriptor);
    } else if (rq->wValue.bytes[1] == USBDESCR_HID) {
        p = (uchar *) (configDescriptor + 18);
        len = 9;
    } else if (rq->wValue.bytes[1] == USBDESCR_HID_REPORT) {
        p = (uchar *) hidReportDescriptor0;
        len = sizeof(hidReportDescriptor0);
    }
    usbMsgPtr = p;
    return len;
}

uchar usbFunctionSetup(uchar data[8]){
    usbRequest_t *rq = (void *) data;

    if ((rq->bmRequestType & USBRQ_TYPE_MASK) == USBRQ_TYPE_CLASS) {    /* class request type */
        if (rq->bRequest == USBRQ_HID_GET_REPORT) {
            /* wValue: ReportType (highbyte), ReportID (lowbyte) */
            if (rq->wValue.bytes[0] == 1) {
                usbMsgPtr = keyboardBuffer;
                return sizeof(keyboardBuffer);
            } else if (rq->wValue.bytes[0] == 2) {
                usbMsgPtr = mouseBuffer;
                return sizeof(mouseBuffer);
            }
            return 0;
        } else if (rq->bRequest == USBRQ_HID_SET_REPORT) {
            /* wValue: ReportType (highbyte), ReportID (lowbyte) */
            /* we have no output/feature reports */
            return 0;
        } else if (rq->bRequest == USBRQ_HID_GET_IDLE) {
            usbMsgPtr = &idleRate;
            return 1;
        } else if (rq->bRequest == USBRQ_HID_SET_IDLE) {
            idleRate = rq->wValue.bytes[1];
        }
        return 0;
    }

    return 0;
}

static void hid_clear(void){
    mouseBuffer[1] = 0;    // mouse buttons
    mouseBuffer[2] = 0;    // X axis delta
    mouseBuffer[3] = 0;    // Y axis delta
    keyboardBuffer[1] = 0;    // keyboard key
}


/* this may well be SIGNAL(SIG_OVERFLOW0), but we want it called at well-defined times */
static void idle_timer(void){ 

    static uchar idleCounter = 0;

    /* idle reports, if requested */
    if (idleRate) {
        if (idleCounter > 4) {
            idleCounter -= 5;
        } else {
            idleCounter = idleRate;
            tx_kbd = 1;
            tx_mouse = 1;
        }
    }
            
//    led_on();
   /* process button releases */
   /* if (ir_timeout) {
        if (!--ir_timeout) {
            led_on();
            hid_clear();
            send_packets();
        }
    }*/
}

/*void ir_queue_poll(void){
    uchar repeat = (irmp_data.flags & IRMP_FLAG_REPETITION);

    // still sending interrupt packet
    if (!usbInterruptIsReady()) {
        return;    
    }

    hid_clear();

    if (repeat) {
        if (ir_accel < IR_ACCEL_MAX / 2)
            ir_accel += IR_ACCEL_INC_LO;
        else if (ir_accel < IR_ACCEL_MAX)
            ir_accel += IR_ACCEL_INC_HI;
    } else {
        ir_accel = IR_ACCEL_DEF;
    }

    switch(ir_data){
    case 0: // Button 0
       keyboardBuffer[1] = 39;
       break;
    case 1: // Button 1
       keyboardBuffer[1] = 30;
       break;
    case 2: // Button 2
       keyboardBuffer[1] = 31;
       break;
    case 3: // Button 3
       keyboardBuffer[1] = 32;
       break;
    case 4: // Button 4
       keyboardBuffer[1] = 33;
       break;
    case 5: // Button 5
       keyboardBuffer[1] = 34;
       break;
    case 6: // Button 6
       keyboardBuffer[1] = 35;
       break;
    case 7: // Button 7
       keyboardBuffer[1] = 36;
       break;
    case 8: // Button 8
       keyboardBuffer[1] = 37;
       break;
    case 9: // Button 9
       keyboardBuffer[1] = 38;
       break;
    case 10: // Key Enter
       keyboardBuffer[1] = 40;
       break;
    case 11: // Key Esc
       keyboardBuffer[1] = 41;
       break;    
    case 12: // PgUp
       keyboardBuffer[1] = 75;
       break;
    case 13: // PgDown
       keyboardBuffer[1] = 78;
       break;       
    case 14: // Mouse Up
       mouseBuffer[3] = -ir_accel; 
       break;
    case 15: // Mouse Down
       mouseBuffer[3] = ir_accel;
       break;
    case 16: // Mouse Left
       mouseBuffer[2] = -ir_accel;
       break;
    case 17: // Mouse Right
       mouseBuffer[2] = ir_accel;
       break;
    case 18: // Full -> mouse button 1
       mouseBuffer[1] |= 1 << (0xf0 & 7);
       break;
    case 19: // Mute -> mouse button 2
       mouseBuffer[1] |= 1 << (0xf1 & 7);
       break;
    case 20: // MTS -> mouse button 3
       mouseBuffer[1] |= 1 << (0xf2 & 7);
       break;       
    default :
        tx_kbd = 1;
        tx_mouse = 1;
        break;
    }

    if (ir_data >= 0 && ir_data <= 13){
        tx_kbd = 1;
    } else if (ir_data >= 14 && ir_data <= 20){
        tx_mouse = 1;
    }
    ir_data = 200;
}*/
    
static void send_packets(void){

    /* send pending packets */
    if (usbInterruptIsReady()) {
        if (tx_kbd) {
            usbSetInterrupt(keyboardBuffer, sizeof(keyboardBuffer)); // send kbd event
            tx_kbd = 0;
        } else if (tx_mouse) {
            usbSetInterrupt(mouseBuffer, sizeof(mouseBuffer)); // send mouse event
            tx_mouse = 0;
        }
    }
}

// timer 1 compare handler, should be called every 1/10000 sec
void TIMER1_COMPA_vect(void) __attribute__((interrupt));

void TIMER1_COMPA_vect(void){
  // main functions for irmp - call irmp ISR
  irmp_ISR();
  // call other timer interrupt routines...
}

uchar are_equal(IRMP_DATA *p1, IRMP_DATA *p2){
    return ((p1->protocol == p2->protocol)  &&  (p1->address == p2->address) &&  (p1->command == p2->command));
}

void usb_print(char *chp) {
  if (*chp){
    if ((*chp >= 'a') && (*chp <= 'z')) {
      keyboardBuffer[1] = *chp - 'a' + 4;
    } else if ((*chp >= 'A') && (*chp <= 'Z')) {
      //buf[0] = MOD_SHIFT_LEFT; // Caps
      keyboardBuffer[1] = *chp - 'A' + 4;
    } else if ((*chp >= '1') && (*chp <= '9')) {
      keyboardBuffer[1] = *chp - '1' + 30;
    } else if (*chp == '0') {
      keyboardBuffer[1] = KEY_0;
    } else {
      switch (*chp) {
        case -1:
          keyboardBuffer[1] = KEY_UP_ARROW; 
          break;
        case -2:
          keyboardBuffer[1] = KEY_DOWN_ARROW; 
          break;
        case -3:
          keyboardBuffer[1] = KEY_LEFT_ARROW; 
          break;
        case -4:
          keyboardBuffer[1] = KEY_RIGHT_ARROW; 
          break;
        case -5:
          keyboardBuffer[1] = KEY_ESCAPE; 
          break;          
        case ' ':
          keyboardBuffer[1] = KEY_SPACE;
          break;
        case '-':
          keyboardBuffer[1] = KEY_MINUS;
          break;
        case '=':
          keyboardBuffer[1] = KEY_EQUALS;
          break;
        case '[':
          keyboardBuffer[1] = KEY_LBRACKET;
          break;
        case ']':
          keyboardBuffer[1] = KEY_RBRACKET;
          break;
        case '\\':
          keyboardBuffer[1] = KEY_BACKSLASH;
          break;
        case ';':
          keyboardBuffer[1] = KEY_SEMICOLON;
          break;
        case ':':
          //buf[0] = MOD_SHIFT_LEFT; // Caps
          keyboardBuffer[1] = KEY_SEMICOLON;
          break;
          case '"':
          keyboardBuffer[1] = KEY_QUOTE;
          break;
        case '~':
          keyboardBuffer[1] = KEY_TILDE;
          break;
        case ',':
          keyboardBuffer[1] = KEY_COMMA;
          break;
        case '/':
          keyboardBuffer[1] = KEY_SLASH;
          break;
        case '\n':
          keyboardBuffer[1] = KEY_ENTER;
          break;
        case '\t':
          keyboardBuffer[1] = KEY_TAB;
          break;
        case '@':
          //buf[0] = MOD_SHIFT_LEFT; // Caps
          keyboardBuffer[1] = KEY_2;
          break;
        default:
          keyboardBuffer[1] = KEY_PERIOD;
          break;
        }
    }
    tx_kbd = 1;
  }
}

// Its frustrating but its f*cking C ... 
void setstr(char **pp, char *p, char v){
  if (v != 0){
    p[0] = v;
    p[1] = 0;
  }
  *pp = p;
}

int main(void) {
  char str[120];
  char *s = "";
  uchar current_codes = 0;
  uchar training_mode = 0;
  uchar i = 0;
  uchar enable_processing = 0;

  wdt_disable();
  gpio_init();

    if (BUTTON_DOWN){
      training_mode = 1;
      s = "\nWelcome in training mode of keysender.\nDevice designed by Grzegorz Hetman.\nPress any keys on remote controler till the end of training mode.\n";
      for (i = 0; i < 10; ++i){
          led_tog();  
          _delay_ms(250);
      }
  }

  wdt_enable(WDTO_2S);

  irmp_init();
  timer_init();
  usb_init();
  hid_clear();

  sei();

  // main event loop
  for (;;) {
    wdt_reset();
    usbPoll();
    // ir signal decoded, do something here...
    // irmp_data.protocol is the protocol, see irmp.h
    // irmp_data.address is the address/manufacturer code of ir sender
    // irmp_data.command is the command code

    // Code for training mode.
    if (training_mode){
      if (current_codes < CODES_TO_TRAIN){
        if ( irmp_get_data (&irmp_data) && (irmp_data.flags & IRMP_FLAG_REPETITION) && (!are_equal(&irmp_data, &last_irmp_data)) ) {
          memcpy(&last_irmp_data, &irmp_data, sizeof(irmp_data));
          //sprintf(str, "\nCurrent code %d address %04x command %04x", current_codes, irmp_data.address, irmp_data.command);
          //sprintf(str, "\nNr %d address %04x command %04x", current_codes, irmp_data.address, irmp_data.command);
          sprintf(str, "\nNr %d for [%s] address %04x command %04x", current_codes, cmd_title[current_codes], irmp_data.address, irmp_data.command);
          // Reset pointer.
          setstr(&s, str, 0);

          i = IR_READY_TO_SAVE;
        } else if (BUTTON_DOWN && i == IR_READY_TO_SAVE) {
          s = " [ OK ]";
          i = 0;
          //sprintf(s, "\nSaving code: %d, address: %04x, command: %04x", current_codes, irmp_data.address, irmp_data.command);
          eeprom_update_block(&irmp_data, &trained_irmp_data[current_codes++], sizeof(irmp_data));
        }
      } else {
        s = " [ OK ]\nWork done - switch to normal mode ...";
        training_mode = 0;
      }

    // Code for regular mode.
    } else if (irmp_get_data (&irmp_data)){
      // Go throught our stored data
      for (i = 0; i < CODES_TO_TRAIN; ++i){
        // Read eeprom value to SRAM last_irmp_data variable.
        eeprom_read_block(&last_irmp_data, &trained_irmp_data[i], sizeof(irmp_data));
        // If match, then set ir_data to current index for future evaluating by ir_queue_poll().
        if (are_equal(&irmp_data, &last_irmp_data)){
          //Process data only when 
          if (enable_processing || i == 0){
            switch(i){
              case 0:
                enable_processing ^= 1;
                break;
              case 1:
                usb_1_tog();
                usb_2_off();
                break;
              case 2:
                usb_1_off();
                usb_2_tog();
                break;
              case 3:
                usb_3_tog();
                break;
              case 4: // UP
                setstr(&s, str, -1);
                break;
              case 5: // DOWN
                setstr(&s, str, -2);
                break;
              case 6: // LEFT
                setstr(&s, str, -3);
                break;
              case 7: // RIGHT
                setstr(&s, str, -4);
                break;
              case 8: // ESC
                setstr(&s, str, -5);
                break;              
              case 9: // ENTER
                s = "\n"; 
                break;
              case 10: // TAB
                s = "\t"; 
                break;                
            }
          }
        }
      }
    } /*else {
      ir_queue_poll();
    }*/
    
    // Ok we send text message as keyboard and do not proccess 
    // IR request till we print all information from buffer.
    if ( *s ){
      if (tx_kbd == 0){
        led_tog();
        usb_print(s++);
      }
    //
    } else if( tx_kbd == 0){
      // If reciver is active then led should be on.
      if (enable_processing){
        led_on();
      } else {
        led_off();  
      }
      hid_clear();
      tx_kbd = 1;
    }

    // On timer overflow (~22ms)
    if (TIFR & _BV(TOV0)) {
        TIFR = _BV(TOV0);
        idle_timer();
    }

    // Send our HID data.
    send_packets();
  }
  return 0;
}